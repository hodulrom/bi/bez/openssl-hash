all:
	mkdir -p bin
	g++ -Wall main.cpp -lcrypto -o bin/main.o

run:
	./bin/main.o ${n}

clean:
	rm -rf bin
