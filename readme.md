# OpenSSL Hash
Assignment number 1 of the 2nd laboratory. This program finds `n` character long string which SHA-256 hash begins with `AABB` (hexa) using random search.

Default length of the generated plain-text is 7 but you can choose whatever length you want by passing an argument (see usage). Value is restricted to be a maximum of 40, otherwise program may crash. 

[EDUX](https://edux.fit.cvut.cz/courses/BI-BEZ/labs/02/start)

## Usage
You can build and run the application via makefile.

**Build**
```bash
make
```
**Run**
```bash
make run
# or with argument
make n=5 run
```
**Clean up build files**
```
make clean
```

## Return codes
* `0`: Success.
* `1`: Program error, hash function or the text-hash pair not found.
* `2`: EVP context could not be created.
* `3`: EVP digest could not be initialized.
* `4`: EVP digest could not be updated.
* `5`: EVP digest could not be finalized.
