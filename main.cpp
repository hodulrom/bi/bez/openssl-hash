#include <stdio.h>
#include <string.h>
#include <openssl/evp.h>

unsigned int seed = (unsigned int) time(NULL);

void generateRandomStr(unsigned char * str, size_t length) {
    for (unsigned int i = 0; i < length; ++i) {
        srand(seed++);

        // Random char in range a..z
        str[i] = (unsigned char) (rand() % ('z' - 'a')) + 'a';
    }
}

int randomHash(
    EVP_MD_CTX * ctx,
    const EVP_MD * type,
    unsigned int textLen,
    unsigned char * outText,
    unsigned char * outHash,
    unsigned int * outHashLen
) {
    int status;

    // Generate some string
    generateRandomStr(outText, textLen);

    status = EVP_DigestInit_ex(ctx, type, NULL);
    if(status != 1) return 3;

    // Feed the message in
    status = EVP_DigestUpdate(ctx, outText, textLen);
    if(status != 1) return 4;

    // Get the hash
    status = EVP_DigestFinal_ex(ctx, outHash, outHashLen);
    if(status != 1) return 5;

    return 1;
}

int main(int argc, char *argv[]) {
    unsigned int i;
    int status;
    unsigned int textLen = 7;
    unsigned char plainText[textLen + 1];
    const char hashFunction[] = "sha256";

    EVP_MD_CTX * ctx;
    const EVP_MD * type;
    unsigned char hash[EVP_MAX_MD_SIZE]; // 64 bytes to fit even the longest (sha 512) hash
    unsigned int hashLen = 0;

    // Accept only single argument (count of characters in generated string)
    if(argc > 2 || (argc == 2 && sscanf(argv[1], "%u", &textLen) != 1) || textLen > 40 || textLen == 0) {
        printf("usage: %s [characters_count 1..40]\n", argv[0]);
        return EXIT_SUCCESS;
    }

    // Init all OpenSSL hash functions.
    OpenSSL_add_all_digests();

    // Determine hash type by string.
    type = EVP_get_digestbyname(hashFunction);

    // In case of unknown hash function name, -1 is returned
    if(!type) {
        printf("Hash function %s does not exist.\n", hashFunction);
        return 1;
    }

    // Context holds hash state and is passed to functions working with the hash
    ctx = EVP_MD_CTX_create();
    if(ctx == NULL) return 2;

    // Find via trying random strings.

    bool found = false;

    for (i = 0; i < 1000000; ++i) {
        status = randomHash(ctx, type, textLen, plainText, hash, & hashLen);

        if(status != 1) {
            return status;
        }

        if(hash[0] == 0xAA && hash[1] == 0xBB) {
            found = true;
            break;
        }
    }

    if(!found) {
        printf("Text not found after a million attempts. Consider using different character count.");
        return 1;
    }

    // Print the found text.
    printf("Hash of \"%s\" is: ", plainText);
    for(i = 0; i < hashLen; i++) {
        printf("%02x", hash[i]);
    }
    printf("\n");

    // Free up resources allocated by context.
    EVP_cleanup();

    return EXIT_SUCCESS;
}
